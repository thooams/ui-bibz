# frozen_string_literal: true

module UiBibz
  NAME                = 'Ui Bibz'
  VERSION             = '2.5.6'
  DESCRIPTION         = 'A Rails Interface Framework using Bootstrap.'
  SUMMARY             = 'Create your project with Ui Bibz. Over a thirty reusable components built to provide iconography, dropdowns, input groups, navigation, alerts, and much more.'
  LICENSE             = 'MIT'
  FONTAWESOME_VERSION = '5.13.0'
  BOOTSTRAP_VERSION   = '4.4.1'
end
