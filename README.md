![Ui Bibz logo](https://repository-images.githubusercontent.com/29547689/3b149e00-a3f6-11e9-9144-e96702055f08)

[![Gem Version](https://badge.fury.io/rb/ui_bibz.svg)](http://badge.fury.io/rb/ui_bibz)
[![Build Status](https://travis-ci.org/thooams/Ui-Bibz.svg)](https://travis-ci.org/thooams/Ui-Bibz)
[![Code Climate](https://codeclimate.com/github/thooams/Ui-Bibz/badges/gpa.svg)](https://codeclimate.com/github/thooams/Ui-Bibz)
[![Test Coverage](https://codeclimate.com/github/thooams/Ui-Bibz/badges/coverage.svg)](https://codeclimate.com/github/thooams/Ui-Bibz)
[![Inline docs](http://inch-ci.org/github/thooams/Ui-Bibz.svg?branch=master)](http://inch-ci.org/github/thooams/Ui-Bibz)
[![security](https://hakiri.io/github/thooams/Ui-Bibz/master.svg)](https://hakiri.io/github/thooams/Ui-Bibz/master)
[![Dependency Status](https://gemnasium.com/thooams/Ui-Bibz.svg)](https://gemnasium.com/thooams/Ui-Bibz)
![Ruby](https://github.com/thooams/Ui-Bibz/workflows/Ruby/badge.svg)

This project rocks and uses MIT-LICENSE.

# Ui Bibz
> Ui Bibz is a [Ui framework](http://fr.wikipedia.org/wiki/Framework_d%27interface)
> to allow build an interface very quickly and simply
> using [Ruby on Rails 4](http://rubyonrails.org/) and [Boostrap 4](http://getbootstrap.com/).

Ui Bibz load [boostrap](http://getbootstrap.com/) and [awesomefont](http://fontawesome.io/).


## Documentation
Full documentation is here :
[Ui Bibz](http://hummel.link/Ui-Bibz/)


## Installation

Add it to your Gemfile
For bootstrap v4:

```
gem 'ui_bibz'
```

For bootstrap v3:

```
gem 'ui_bibz', '~> 1.2.5.3'
```

Run the following command to install it:

```
bundle install
```

## Configuration

Put this line in: /app/assets/stylesheets/applications.css

```
*= require ui_bibz
```

Put this line in: /app/assets/javascripts/applications.js

```
//= require ui_bibz
```

Ps: You can use sass variables into Boostrap.

## And after

* See documentation [here](https://ui-bibz-documentation.herokuapp.com/) for boostrap 4.
* See documentation [here](http://hummel.link/Ui-Bibz/1.2.5/) for boostrap 3.

